<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerCountryController extends Controller
{
    /**
     * @Route("/oneWeekPerCountry", name="analyticsOneWeekPerCountry")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekCountryAction(Request $request){
        $sevenDaysPerCountry = $this->get('gkratz_analytic_charts')->sevenDaysPerCountry();
        $pieOneWeekPerCountry = $this->get('gkratz_analytic_charts')->pieOneWeekPerCountry();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerCountry.html.twig', array(
            'sevenDaysPerCountry' => $sevenDaysPerCountry,
            'pieOneWeekPerCountry' => $pieOneWeekPerCountry
        ));
    }

    /**
     * @Route("/oneYearPerCountry", name="analyticsOneYearPerCountry")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearCountryAction(Request $request){
        $oneYearPerCountry = $this->get('gkratz_analytic_charts')->oneYearPerCountry();
        $pieOneYearPerCountry = $this->get('gkratz_analytic_charts')->pieOneYearPerCountry();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerCountry.html.twig', array(
            'oneYearPerCountry' => $oneYearPerCountry,
            'pieOneYearPerCountry' => $pieOneYearPerCountry
        ));
    }
}