![](https://bitbucket.org/jul6art/gkratzanalyticbundle/raw/master/Resources/public/assets/screenshot.png)

## IMPORTANT NOTICE

We recommend to use a mariadb database if you have errors with sql queries.
The bundle use complexes sql queries and mariadb is better than mysql in this case.


## Installation

1) //-- add in the composer.json of your project --//

    "require": {
        // ...
        "beberlei/DoctrineExtensions": "dev-master",
        "gkratz/analyticbundle": "dev-master"
    }


2) //-- activate the bundle in your app/AppKernel.php --//

    new Gkratz\AnalyticBundle\GkratzAnalyticBundle(),
    new Ob\HighchartsBundle\ObHighchartsBundle(),
    new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
    new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),


3) //-- enter this command in your terminal and after installation, verify if the beberlei folder is present in the vendor folder. If no, enter this command after: php composer update beberlei/DoctrineExtensions --//

    php composer update gkratz/analyticbundle


4) //-- create an empty AppBundle:Analytic entity with a PROTECTED id and that extends \Gkratz\AnalyticBundle\Model\Analytic --//

    <?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Gkratz\AnalyticBundle\Model\Analytic as BaseAnalytic;

    /**
     * Analytic
     *
     * @ORM\Table(name="analytic")
     * @ORM\Entity(repositoryClass="AppBundle\Repository\AnalyticRepository")
     */
    class Analytic extends BaseAnalytic
    {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;


        /**
         * Get id
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        public function __construct()
        {
            parent::__construct();
        }
    }


5) //-- enter these commands --//

    php bin/console doctrine:generate:entities App
    php bin/console doctrine:schema:update --force


6) //-- update your front controller --//
   //-- use only render() method to render view, not renderView() --//

    <?php

    namespace AppBundle\Controller;

    use Gkratz\AnalyticBundle\Controller\GKratzAnalyticController;

    class DefaultController extends GKratzAnalyticController
    {
        // ...
    }


7) //-- insert the config in app/config/config.yml --//

    # Twig Configuration
    twig:
        # ...
        form_themes:
            - LexikFormFilterBundle:Form:form_div_layout.html.twig

    # Doctrine Configuration
    doctrine:
        # ...
        orm:
            # ...
            dql:
                datetime_functions:
                    month: DoctrineExtensions\Query\Mysql\Month
                    year: DoctrineExtensions\Query\Mysql\Year
                    day: DoctrineExtensions\Query\Mysql\Day
                    dayofweek: DoctrineExtensions\Query\Mysql\DayOfWeek

    # KNPPaginatorBundle Configuration
    knp_paginator:
        default_options:
            distinct: false
        template:
            pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig


## How to use

1) //-- in any action of any controller, give a chart to your view --//

    $oneYearPerBrowser = $this->get('gkratz_analytic_charts')->oneYearPerBrowser();

    return $this->render('default/analytics.html.twig', array(
                'oneYearPerBrowser' => $oneYearPerBrowser
                )
    );


2) //-- in your view, define areas and import js after jquery import (necessary) --//

    <div id="oneYearPerBrowser" class="myGCharts"></div>
    <script type="text/javascript" src="{{ asset('bundles/gkratzanalytic/js/myChart.js') }}">
    </script>
    <script type="text/javascript">
        {{ chart(oneYearPerBrowser) }}
    </script>


## Use the bundle complete table and pages (like the demo screenshot)

1) update your app/config/routing.yml:

    GkratzAnalytic:
        resource: "@GkratzAnalyticBundle/Controller/"
        type:     annotation
        prefix:   /


2) make sure that this line in config.yml is decommented to enable translations (available in 'FR' and 'EN')

    framework:
        translator:      { fallbacks: ["%locale%"] }


3) visite the page at yoursite/analytics


## Override

    - you can change the number of items per page in the
      vendor\gkratz\analyticbundle\Gkratz\AnalyticBundle\Constants\Constants.php file

    - you can change the prefix of routes in bundle routing in your routing.yml file

    - you can define an access control in your security.yml file
      ie: access_control:
              - { path: ^/analytics/, roles: ROLE_ADMIN }

    - you can insert translations for your desired language or override existing translations
      in a well formatted messages.{{ lang}}.xliff in your app\Resources\translations folder
      original files: vendor\gkratz\analyticbundle\Gkratz\AnalyticBundle\Resources\translations
      (clean the cache to see effect)

    - you can override the html files like copying it in a
      app\Resources\GkratzAnalyticBundle\views\default directory and extending your layout.
      You need to rename the gkratz block for your case and you need to get the js files.


## All the graphs

    // one year per new or returning visitor
    $oneYearPerUser = $this->get('gkratz_analytic_charts')->oneYearPerUser();
    <div id="oneYearPerUser" class="myGCharts"></div>
    {{ chart(oneYearPerUser) }}

    // one year per country
    $oneYearPerCountry = $this->get('gkratz_analytic_charts')->oneYearPerCountry();
    <div id="oneYearPerCountry" class="myGCharts"></div>
    {{ chart(oneYearPerCountry) }}

    // one year per city
    $oneYearPerCity = $this->get('gkratz_analytic_charts')->oneYearPerCity();
    <div id="oneYearPerCity" class="myGCharts"></div>
    {{ chart(oneYearPerCity) }}

    // one year per language
    $oneYearPerLanguage = $this->get('gkratz_analytic_charts')->oneYearPerLanguage();
    <div id="oneYearPerLanguage" class="myGCharts"></div>
    {{ chart(oneYearPerLanguage) }}

    // one year per browser
    $oneYearPerBrowser = $this->get('gkratz_analytic_charts')->oneYearPerBrowser();
    <div id="oneYearPerBrowser" class="myGCharts"></div>
    {{ chart(oneYearPerBrowser) }}

    // one year per pages views
    $oneYearPerPage = $this->get('gkratz_analytic_charts')->oneYearPerPage();
    <div id="oneYearPerPage" class="myGCharts"></div>
    {{ chart(oneYearPerPage) }}

    // one year pages views
    $oneYearPages = $this->get('gkratz_analytic_charts')->oneYearPages();
    <div id="oneYearPages" class="myGCharts"></div>
    {{ chart(oneYearPages) }}

    // 7 days by new or returning visitor
    $sevenDaysPerUser = $this->get('gkratz_analytic_charts')->sevenDaysPerUser();
    <div id="sevenDaysPerUser" class="myGCharts"></div>
    {{ chart(sevenDaysPerUser) }}

    // 7 days per country
    $sevenDaysPerCountry = $this->get('gkratz_analytic_charts')->sevenDaysPerCountry();
    <div id="sevenDaysPerCountry" class="myGCharts"></div>
    {{ chart(sevenDaysPerCountry) }}

    // 7 days per city
    $sevenDaysPerCity = $this->get('gkratz_analytic_charts')->sevenDaysPerCity();
    <div id="sevenDaysPerCity" class="myGCharts"></div>
    {{ chart(sevenDaysPerCity) }}

    // 7 days per language
    $sevenDaysPerLanguage = $this->get('gkratz_analytic_charts')->sevenDaysPerLanguage();
    <div id="sevenDaysPerLanguage" class="myGCharts"></div>
    {{ chart(sevenDaysPerLanguage) }}

    // 7 days per browser
    $sevenDaysPerBrowser = $this->get('gkratz_analytic_charts')->sevenDaysPerBrowser();
    <div id="sevenDaysPerBrowser" class="myGCharts"></div>
    {{ chart(sevenDaysPerBrowser) }}

    // 7 days per pages views
    $sevenDaysPerPage = $this->get('gkratz_analytic_charts')->sevenDaysPerPage();
    <div id="sevenDaysPerPage" class="myGCharts"></div>
    {{ chart(sevenDaysPerPage) }}

    // 7 days pages views
    $sevenDaysPages = $this->get('gkratz_analytic_charts')->sevenDaysPages();
    <div id="sevenDaysPages" class="myGCharts"></div>
    {{ chart(sevenDaysPages) }}

    // pie one year per country
    $pieOneYearPerCountry = $this->get('gkratz_analytic_charts')->pieOneYearPerCountry();
    <div id="pieOneYearPerCountry" class="myGCharts"></div>
    {{ chart(pieOneYearPerCountry) }}

    // pie one year per city
    $pieOneYearPerCity = $this->get('gkratz_analytic_charts')->pieOneYearPerCity();
    <div id="pieOneYearPerCity" class="myGCharts"></div>
    {{ chart(pieOneYearPerCity) }}

    // pie one year per language
    $pieOneYearPerLanguage = $this->get('gkratz_analytic_charts')->pieOneYearPerLanguage();
    <div id="pieOneYearPerLanguage" class="myGCharts"></div>
    {{ chart(pieOneYearPerLanguage) }}

    // pie one year per browser
    $pieOneYearPerBrowser = $this->get('gkratz_analytic_charts')->pieOneYearPerBrowser();
    <div id="pieOneYearPerBrowser" class="myGCharts"></div>
    {{ chart(pieOneYearPerBrowser) }}

    // pie one year per page
    $pieOneYearPerPage = $this->get('gkratz_analytic_charts')->pieOneYearPerPage();
    <div id="pieOneYearPerPage" class="myGCharts"></div>
    {{ chart(pieOneYearPerPage) }}

    // pie one week per country
    $pieOneWeekPerCountry = $this->get('gkratz_analytic_charts')->pieOneWeekPerCountry();
    <div id="pieOneWeekPerCountry" class="myGCharts"></div>
    {{ chart(pieOneWeekPerCountry) }}

    // pie one week per city
    $pieOneWeekPerCity = $this->get('gkratz_analytic_charts')->pieOneWeekPerCity();
    <div id="pieOneWeekPerCity" class="myGCharts"></div>
    {{ chart(pieOneWeekPerCity) }}

    // pie one week per language
    $pieOneWeekPerLanguage = $this->get('gkratz_analytic_charts')->pieOneWeekPerLanguage();
    <div id="pieOneWeekPerLanguage" class="myGCharts"></div>
    {{ chart(pieOneWeekPerLanguage) }}

    // pie one week per browser
    $pieOneWeekPerBrowser = $this->get('gkratz_analytic_charts')->pieOneWeekPerBrowser();
    <div id="pieOneWeekPerBrowser" class="myGCharts"></div>
    {{ chart(pieOneWeekPerBrowser) }}

    // pie one week per page
    $pieOneWeekPerPage = $this->get('gkratz_analytic_charts')->pieOneWeekPerPage();
    <div id="pieOneWeekPerPage" class="myGCharts"></div>
    {{ chart(pieOneWeekPerPage) }}