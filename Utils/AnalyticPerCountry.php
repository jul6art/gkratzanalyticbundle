<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerCountry extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function countrySevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.country as country, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('country, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $countries = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['country'], $countries)){
                    $countries[] = $entity['country'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["country"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['country']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($countries as $country){
            $series[] = array("name" => $translator->trans($country), "data" => array($params[1][$country], $params[2][$country], $params[3][$country], $params[4][$country], $params[5][$country], $params[6][$country], $params[0][$country]));
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'sevenDaysPerCountry';
        $text = $translator->trans("Visits of the week per country");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per country"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function countryOneWeek(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.country as country, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('country, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $countries = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['country'], $countries)){
                    $countries[] = $entity['country'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["country"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['country']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($countries as $country){
            $data[] = array($translator->trans($country), $params[1][$country] + $params[2][$country] + $params[3][$country] + $params[4][$country] + $params[5][$country] + $params[6][$country] + $params[0][$country]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'pieOneWeekPerCountry';
        $text = $translator->trans("Visits of the week per country");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per country"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array(
                "enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function countryOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.country as country, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('country, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $countries = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['country'], $countries)){
                    $countries[] = $entity['country'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["country"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['country']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($countries as $country){
            $series[] = array("name" => $translator->trans($country), "data" => array($params[1][$country], $params[2][$country], $params[3][$country], $params[4][$country], $params[5][$country], $params[6][$country], $params[7][$country], $params[8][$country], $params[9][$country], $params[10][$country], $params[11][$country], $params[0][$country]));
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'oneYearPerCountry';
        $text = $translator->trans("Visits of the year per country");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per country"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function countryPieOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.country as country, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('country, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $countries = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['country'], $countries)){
                    $countries[] = $entity['country'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["country"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['country']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($countries as $country){
            $data[] = array($translator->trans($country), $params[1][$country] + $params[2][$country] + $params[3][$country] + $params[4][$country] + $params[5][$country] + $params[6][$country] + $params[7][$country] + $params[8][$country] + $params[9][$country] + $params[10][$country] + $params[11][$country] + $params[0][$country]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'pieOneYearPerCountry';
        $text = $translator->trans("Visits of the year per country");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per country"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }
}