<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKTableController extends Controller
{
    /**
     * @Route("/", name="analyticsTable")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tableAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(\Gkratz\AnalyticBundle\Form\AnalyticFilterType::class, null, ['em' => $em]);

        /** @var  $repo \AppBundle\Repository\AnalyticRepository */
        $repo = $this->getDoctrine()->getRepository(\AppBundle\Entity\Analytic::class);
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $repo->createQueryBuilder('a');

        $itemsPerPage = Constants::ITEM_PER_PAGE;

        $page = $request->query->get('page', 1);

        $sort = 'a.id';
        if ($request->query->has('sort')){
            $sort = $request->query->get('sort');
        }
        $direction = ($request->query->has('direction')) ? $request->query->get('direction') : 'asc';

        $form->handleRequest($request);
        if ($form->isSubmitted()&&!$form->isValid()) {
            $entities = NULL;
        } else {
            if ($form->isSubmitted()) {
                // save data in session
                $session = $this->get('session');
                $session->set($form->getName(), $request->request->get($form->getName()));
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                // $itemsPerPage = $form->get('itemsPerPage')->get('itemsPerPage')->getData();
                // Reset page number after search
                $page = 1;
            } else {
                // not submit get data in session
                $form = $this->createForm(\Gkratz\AnalyticBundle\Form\AnalyticFilterType::class, null, ['em' => $em]);
                $session = $request->getSession();
                if ($this->keepFilterForm($request,$form)){
                    $form->submit($session->get($form->getName()));
                    //   $itemsPerPage = $form->get('itemsPerPage')->get('itemsPerPage')->getData();
                    $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
                }
            }
            $paginator = $this->get('knp_paginator');
            $entities = $paginator->paginate($query, $page/* page number */, $itemsPerPage, array('defaultSortFieldName' => $sort,
                    'defaultSortDirection' => $direction, 'wrap-queries' => true)
            );
        }
        return $this->render('GkratzAnalyticBundle:default:table.html.twig', array(
            'pagination' => $entities
        , 'form' => $form->createView()
        ));
    }

    /**
     * @param $request
     * @param $form
     * @return bool
     */
    protected function keepFilterForm($request, $form){
        $keepForm = ($request->getSession()->get($form->getName()) != NULL);//&&($request->query->has('sort')||$request->query->has('page'));
        if (!$keepForm){
            $request->getSession()->remove($form->getName());
        }

        return $keepForm;
    }
}