<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerLanguage extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function languageSevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.language as language, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('language, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $langs = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['language'], $langs)){
                    $langs[] = $entity['language'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["language"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['language']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($langs as $lang){
            $series[] = array("name" => $translator->trans($lang), "data" => array($params[1][$lang], $params[2][$lang], $params[3][$lang], $params[4][$lang], $params[5][$lang], $params[6][$lang], $params[0][$lang]));
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'sevenDaysPerLanguage';
        $text = $translator->trans("Visits of the week per language");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per language"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function languageOneWeek(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.language as language, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('language, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $langs = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['language'], $langs)){
                    $langs[] = $entity['language'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["language"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['language']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($langs as $lang){
            $data[] = array($translator->trans($lang), $params[1][$lang] + $params[2][$lang] + $params[3][$lang] + $params[4][$lang] + $params[5][$lang] + $params[6][$lang] + $params[0][$lang]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'pieOneWeekPerLanguage';
        $text = $translator->trans("Visits of the week per language");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per language"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array(
                "enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function languageOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.language as language, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('language, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $langs = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['language'], $langs)){
                    $langs[] = $entity['language'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["language"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['language']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($langs as $lang){
            $series[] = array("name" => $translator->trans($lang), "data" => array($params[1][$lang], $params[2][$lang], $params[3][$lang], $params[4][$lang], $params[5][$lang], $params[6][$lang], $params[7][$lang], $params[8][$lang], $params[9][$lang], $params[10][$lang], $params[11][$lang], $params[0][$lang]));
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'oneYearPerLanguage';
        $text = $translator->trans("Visits of the year per language");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per language"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function languagePieOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.language as language, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('language, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $langs = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['language'], $langs)){
                    $langs[] = $entity['language'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["language"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['language']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($langs as $lang){
            $data[] = array($translator->trans($lang), $params[1][$lang] + $params[2][$lang] + $params[3][$lang] + $params[4][$lang] + $params[5][$lang] + $params[6][$lang] + $params[7][$lang] + $params[8][$lang] + $params[9][$lang] + $params[10][$lang] + $params[11][$lang] + $params[0][$lang]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'pieOneYearPerLanguage';
        $text = $translator->trans("Visits of the year per language");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per language"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }
}