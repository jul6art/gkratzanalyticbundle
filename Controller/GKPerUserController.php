<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerUserController extends Controller
{
    /**
     * @Route("/oneWeekPerUser", name="analyticsOneWeekPerUser")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekUserAction(Request $request){
        $sevenDaysPerUser = $this->get('gkratz_analytic_charts')->sevenDaysPerUser();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerUser.html.twig', array(
            'sevenDaysPerUser' => $sevenDaysPerUser
        ));
    }

    /**
     * @Route("/oneYearPerUser", name="analyticsOneYearPerUser")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearUserAction(Request $request){
        $oneYearPerUser = $this->get('gkratz_analytic_charts')->oneYearPerUser();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerUser.html.twig', array(
            'oneYearPerUser' => $oneYearPerUser
        ));
    }
}