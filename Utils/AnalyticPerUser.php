<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerUser extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function userSevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.ip as ip, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, a.newSession as new_session')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        $qb2 = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.ip as ip, COUNT(a.id) as records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->groupBy('a.ip')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->addOrderBy('a.date', 'desc');
        $group2 = $qb2->getQuery()->getResult();
        $group3 = array();
        foreach($group2 as $key => $elem){
            $group3[$elem["ip"]] = $elem["records"];
        }

        //init
        $sorts = array('new' => 0, 'ret' => 0);
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if($group3[$entity["ip"]] == 1){
                    $params[$offset]['new'] += 1;
                }else{
                    $params[$offset]['ret'] += 1;
                }
            }
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //set series
        $text1 = $translator->trans("New visitor");
        $text2 = $translator->trans("Returning visitor");
        $series = array(
            array(
                'name'  => $text1,
                'type'  => 'column',
                'color' => '#4572A7',
                'data'  => array($params[1]['new'],$params[2]['new'],$params[3]['new'],$params[4]['new'],$params[5]['new'],$params[6]['new'],$params[0]['new']),
            ),
            array(
                'name'  => $text2,
                'type'  => 'column',
                'color' => '#AA4643',
                'data'  => array($params[1]['ret'],$params[2]['ret'],$params[3]['ret'],$params[4]['ret'],$params[5]['ret'],$params[6]['ret'],$params[0]['ret']),
            ),
        );

        //render chart
        $yData = array(
            array(
                'labels' => array(
                    'style'     => array('color' => '#AA4643')
                ),
                'title' => array(
                    'text'  => $text2,
                    'style' => array('color' => '#AA4643')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#4572A7')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text'  => $text1,
                    'style' => array('color' => '#4572A7')
                ),
            ),
        );
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = new Highchart();
        $chart->chart->renderTo('sevenDaysPerUser'); // The #id of the div where to render the chart
        $chart->chart->type('column');
        $chart->title->text($translator->trans("Visits of the week"));
        $chart->xAxis->categories($categories);
        $chart->yAxis($yData);
        $chart->legend->enabled(false);
        $chart->series($series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function userOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.ip as ip, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, a.newSession as new_session')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        $qb2 = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.ip as ip, COUNT(a.id) as records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->groupBy('a.ip')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->addOrderBy('a.date', 'desc');
        $group2 = $qb2->getQuery()->getResult();
        $group3 = array();
        foreach($group2 as $key => $elem){
            $group3[$elem["ip"]] = $elem["records"];
        }

        //init
        $sorts = array('new' => 0, 'ret' => 0);
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if($group3[$entity["ip"]] == 1){
                    $params[$offset]['new'] += 1;
                }else{
                    $params[$offset]['ret'] += 1;
                }
            }
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //set series
        $text1 = $translator->trans("New visitor");
        $text2 = $translator->trans("Returning visitor");
        $series = array(
            array(
                'name'  => $text1,
                'type'  => 'column',
                'color' => '#4572A7',
                'data'  => array($params[1]['new'],$params[2]['new'],$params[3]['new'],$params[4]['new'],$params[5]['new'],$params[6]['new'],$params[7]['new'],$params[8]['new'],$params[9]['new'],$params[10]['new'],$params[11]['new'],$params[0]['new']),
            ),
            array(
                'name'  => $text2,
                'type'  => 'column',
                'color' => '#AA4643',
                'data'  => array($params[1]['ret'],$params[2]['ret'],$params[3]['ret'],$params[4]['ret'],$params[5]['ret'],$params[6]['ret'],$params[7]['ret'],$params[8]['ret'],$params[9]['ret'],$params[10]['ret'],$params[11]['ret'],$params[0]['ret']),
            ),
        );

        //render chart
        $yData = array(
            array(
                'labels' => array(
                    'style'     => array('color' => '#AA4643')
                ),
                'title' => array(
                    'text'  => $text2,
                    'style' => array('color' => '#AA4643')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#4572A7')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text'  => $text1,
                    'style' => array('color' => '#4572A7')
                ),
            ),
        );
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = new Highchart();
        $chart->chart->renderTo('oneYearPerUser'); // The #id of the div where to render the chart
        $chart->chart->type('column');
        $chart->title->text($translator->trans("Visits of the year"));
        $chart->xAxis->categories($categories);
        $chart->yAxis($yData);
        $chart->legend->enabled(false);
        $chart->series($series);
        return $chart;
    }
}