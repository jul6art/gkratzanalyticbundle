<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerLanguageController extends Controller
{
    /**
     * @Route("/oneWeekPerLanguage", name="analyticsOneWeekPerLanguage")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekLanguageAction(Request $request){
        $sevenDaysPerLanguage = $this->get('gkratz_analytic_charts')->sevenDaysPerLanguage();
        $pieOneWeekPerLanguage = $this->get('gkratz_analytic_charts')->pieOneWeekPerLanguage();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerLanguage.html.twig', array(
            'sevenDaysPerLanguage' => $sevenDaysPerLanguage,
            'pieOneWeekPerLanguage' => $pieOneWeekPerLanguage
        ));
    }

    /**
     * @Route("/oneYearPerLanguage", name="analyticsOneYearPerLanguage")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearLanguageAction(Request $request){
        $oneYearPerLanguage = $this->get('gkratz_analytic_charts')->oneYearPerLanguage();
        $pieOneYearPerLanguage = $this->get('gkratz_analytic_charts')->pieOneYearPerLanguage();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerLanguage.html.twig', array(
            'oneYearPerLanguage' => $oneYearPerLanguage,
            'pieOneYearPerLanguage' => $pieOneYearPerLanguage
        ));
    }
}