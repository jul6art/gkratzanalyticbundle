<?php



namespace Gkratz\AnalyticBundle\Form\Standard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of DateTimePickerRangeType
 *
 * @author erandre
 */
class DateTimePickerRangeType extends AbstractType{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('left_datetime', 'Gkratz\AnalyticBundle\Form\Standard\DateTimePickerType', $options['left_datetime_options']);
        $builder->add('right_datetime', 'Gkratz\AnalyticBundle\Form\Standard\DateTimePickerType', $options['right_datetime_options']);
        $builder->setAttribute('filter_value_keys', array(
            'left_datetime'  => $options['left_datetime_options'],
            'right_datetime' => $options['right_datetime_options'],
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'required'               => false,
                'left_datetime_options'  => array(),
                'right_datetime_options' => array(),
                'data_extraction_method' => 'value_keys',
                'widget' => 'single_text',
            ))
            ->setAllowedValues('data_extraction_method', array('value_keys'))
        ;
    }
    
}
