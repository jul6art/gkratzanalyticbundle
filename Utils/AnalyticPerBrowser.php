<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerBrowser extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function browserSevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.browser as browser, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('browser, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('moz' => 0, 'chr' => 0, 'div' => 0);
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                $params[$offset][$entity['browser']] += $entity["records"];
            }
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //set series
        $series = array(
            array(
                'name'  => $translator->trans("moz"),
                'type'  => 'column',
                'yAxis' => 1,
                'color' => '#7CB5EC',
                'data'  => array($params[1]['moz'],$params[2]['moz'],$params[3]['moz'],$params[4]['moz'],$params[5]['moz'],$params[6]['moz'],$params[0]['moz']),
            ),
            array(
                'name'  => $translator->trans("chr"),
                'type'  => 'column',
                'yAxis' => 1,
                'color' => '#434348',
                'data'  => array($params[1]['chr'],$params[2]['chr'],$params[3]['chr'],$params[4]['chr'],$params[5]['chr'],$params[6]['chr'],$params[0]['chr']),
            ),
            array(
                'name'  => $translator->trans("div"),
                'type'  => 'spline',
                'yAxis' => 1,
                'color' => '#90ED7D',
                'data'  => array($params[1]['div'],$params[2]['div'],$params[3]['div'],$params[4]['div'],$params[5]['div'],$params[6]['div'],$params[0]['div']),
            ),
        );

        //render chart
        $yData = array(
            array(
                'labels' => array(
                    'style'     => array('color' => '#90ED7D')
                ),
                'title' => array(
                    'text'  => $translator->trans("div"),
                    'style' => array('color' => '#90ED7D')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#434348')
                ),
                'title' => array(
                    'text'  => $translator->trans("chr"),
                    'style' => array('color' => '#434348')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#7CB5EC')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text'  => $translator->trans("moz"),
                    'style' => array('color' => '#7CB5EC')
                ),
            ),
        );
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = new Highchart();
        $chart->chart->renderTo('sevenDaysPerBrowser');
        $chart->chart->type('column');
        $chart->title->text($translator->trans("Visits of the week per browser"));
        $chart->xAxis->categories($categories);
        $chart->yAxis($yData);
        $chart->legend->enabled(false);
        $chart->series($series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function browserOneWeek(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.browser as browser, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('browser, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('moz' => 0, 'chr' => 0, 'div' => 0);
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                $params[$offset][$entity['browser']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        $data[] = array($translator->trans("moz"), $params[1]['moz'] + $params[2]['moz'] + $params[3]['moz'] + $params[4]['moz'] + $params[5]['moz'] + $params[6]['moz'] + $params[0]['moz']);
        $data[] = array($translator->trans("chr"), $params[1]['chr'] + $params[2]['chr'] + $params[3]['chr'] + $params[4]['chr'] + $params[5]['chr'] + $params[6]['chr'] + $params[0]['chr']);
        $data[] = array($translator->trans("div"), $params[1]['div'] + $params[2]['div'] + $params[3]['div'] + $params[4]['div'] + $params[5]['div'] + $params[6]['div'] + $params[0]['div']);
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'pieOneWeekPerBrowser';
        $text = $translator->trans("Visits of the week per browser");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per browser"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function browserOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db manager
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.browser as browser, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('browser, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('moz' => 0, 'chr' => 0, 'div' => 0);
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                $params[$offset][$entity['browser']] += $entity["records"];
            }
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //set series
        $series = array(
            array(
                'name'  => $translator->trans("moz"),
                'type'  => 'column',
                'yAxis' => 1,
                'color' => '#7CB5EC',
                'data'  => array($params[1]['moz'],$params[2]['moz'],$params[3]['moz'],$params[4]['moz'],$params[5]['moz'],$params[6]['moz'],$params[7]['moz'],$params[8]['moz'],$params[9]['moz'],$params[10]['moz'],$params[11]['moz'],$params[0]['moz']),
            ),
            array(
                'name'  => $translator->trans("chr"),
                'type'  => 'column',
                'yAxis' => 1,
                'color' => '#434348',
                'data'  => array($params[1]['chr'],$params[2]['chr'],$params[3]['chr'],$params[4]['chr'],$params[5]['chr'],$params[6]['chr'],$params[7]['chr'],$params[8]['chr'],$params[9]['chr'],$params[10]['chr'],$params[11]['chr'],$params[0]['chr']),
            ),
            array(
                'name'  => $translator->trans("div"),
                'type'  => 'spline',
                'yAxis' => 1,
                'color' => '#90ED7D',
                'data'  => array($params[1]['div'],$params[2]['div'],$params[3]['div'],$params[4]['div'],$params[5]['div'],$params[6]['div'],$params[7]['div'],$params[8]['div'],$params[9]['div'],$params[10]['div'],$params[11]['div'],$params[0]['div']),
            ),
        );

        //render chart
        $yData = array(
            array(
                'labels' => array(
                    'style'     => array('color' => '#90ED7D')
                ),
                'title' => array(
                    'text'  => $translator->trans("div"),
                    'style' => array('color' => '#90ED7D')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#434348')
                ),
                'title' => array(
                    'text'  => $translator->trans("chr"),
                    'style' => array('color' => '#434348')
                ),
                'opposite' => true,
            ),
            array(
                'labels' => array(
                    'style'     => array('color' => '#7CB5EC')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text'  => $translator->trans("moz"),
                    'style' => array('color' => '#7CB5EC')
                ),
            ),
        );
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = new Highchart();
        $chart->chart->renderTo('oneYearPerBrowser'); // The #id of the div where to render the chart
        $chart->chart->type('column');
        $chart->title->text($translator->trans("Visits of the year per browser"));
        $chart->xAxis->categories($categories);
        $chart->yAxis($yData);
        $chart->legend->enabled(false);
        $chart->series($series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function browserPieOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.browser as browser, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('browser, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('moz' => 0, 'chr' => 0, 'div' => 0);
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                $params[$offset][$entity['browser']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        $data[] = array($translator->trans("moz"), $params[1]['moz'] + $params[2]['moz'] + $params[3]['moz'] + $params[4]['moz'] + $params[5]['moz'] + $params[6]['moz'] + $params[7]['moz'] + $params[8]['moz'] + $params[9]['moz'] + $params[10]['moz'] + $params[11]['moz'] + $params[0]['moz']);
        $data[] = array($translator->trans("chr"), $params[1]['chr'] + $params[2]['chr'] + $params[3]['chr'] + $params[4]['chr'] + $params[5]['chr'] + $params[6]['chr'] + $params[7]['chr'] + $params[8]['chr'] + $params[9]['chr'] + $params[10]['chr'] + $params[11]['chr'] + $params[0]['chr']);
        $data[] = array($translator->trans("div"), $params[1]['div'] + $params[2]['div'] + $params[3]['div'] + $params[4]['div'] + $params[5]['div'] + $params[6]['div'] + $params[7]['div'] + $params[8]['div'] + $params[9]['div'] + $params[10]['div'] + $params[11]['div'] + $params[0]['div']);
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'pieOneYearPerBrowser';
        $text = $translator->trans("Visits of the year per browser");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per browser"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }
}