<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 01/11/2016
 * Time: 23:26
 */

namespace Gkratz\AnalyticBundle\Model;


/**
 * Interface AnalyticInterface
 * @package Gkratz\AnalyticBundle\Model
 */
interface AnalyticInterface
{
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Analytic
     */
    public function setDate($date);

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Set page
     *
     * @param string $page
     * @return Analytic
     */
    public function setPage($page);

    /**
     * Get page
     *
     * @return string
     */
    public function getPage();

    /**
     * Set ip
     *
     * @param string $ip
     * @return Analytic
     */
    public function setIp($ip);

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp();

    /**
     * Set country
     *
     * @param string $ip
     * @return Analytic
     */
    public function setCountry($country);

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Set city
     *
     * @param string $city
     * @return Analytic
     */
    public function setCity($city);

    /**
     * Get city
     *
     * @return string
     */
    public function getCity();

    /**
     * Set host
     *
     * @param string $host
     * @return Analytic
     */
    public function setHost($host);

    /**
     * Get host
     *
     * @return string
     */
    public function getHost();

    /**
     * Set browser
     *
     * @param string $browser
     * @return Analytic
     */
    public function setBrowser($browser);

    /**
     * Get browser
     *
     * @return string
     */
    public function getBrowser();

    /**
     * Set language
     *
     * @param string $language
     * @return Analytic
     */
    public function setLanguage($language);

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage();

    /**
     * Set referer
     *
     * @param string $referer
     * @return Analytic
     */
    public function setReferer($referer);

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer();

    /**
     * Set newSession
     *
     * @param boolean $newSession
     * @return Analytic
     */
    public function setNewSession($newSession);

    /**
     * Get newSession
     *
     * @return boolean
     */
    public function getNewSession();
}