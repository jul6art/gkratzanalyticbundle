<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerCity extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function citySevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.city as city, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('city, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $cities = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['city'], $cities)){
                    $cities[] = $entity['city'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["city"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['city']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($cities as $city){
            $series[] = array("name" => $translator->trans($city), "data" => array($params[1][$city], $params[2][$city], $params[3][$city], $params[4][$city], $params[5][$city], $params[6][$city], $params[0][$city]));
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'sevenDaysPerCity';
        $text = $translator->trans("Visits of the week per city");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per city"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function cityOneWeek(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.city as city, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('city, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $cities = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['city'], $cities)){
                    $cities[] = $entity['city'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["city"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['city']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($cities as $city){
            $data[] = array($translator->trans($city), $params[1][$city] + $params[2][$city] + $params[3][$city] + $params[4][$city] + $params[5][$city] + $params[6][$city] + $params[0][$city]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'pieOneWeekPerCity';
        $text = $translator->trans("Visits of the week per city");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Visits per city"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array(
                "enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function cityOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.city as city, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('city, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $cities = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['city'], $cities)){
                    $cities[] = $entity['city'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["city"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['city']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($cities as $city){
            $series[] = array("name" => $translator->trans($city), "data" => array($params[1][$city], $params[2][$city], $params[3][$city], $params[4][$city], $params[5][$city], $params[6][$city], $params[7][$city], $params[8][$city], $params[9][$city], $params[10][$city], $params[11][$city], $params[0][$city]));
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'oneYearPerCity';
        $text = $translator->trans("Visits of the year per city");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per city"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function cityPieOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.city as city, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.newSession = :newSession')
            ->andWhere('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2, 'newSession' => 1))
            ->groupBy('city, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $cities = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['city'], $cities)){
                    $cities[] = $entity['city'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["city"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['city']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($cities as $city){
            $data[] = array($translator->trans($city), $params[1][$city] + $params[2][$city] + $params[3][$city] + $params[4][$city] + $params[5][$city] + $params[6][$city] + $params[7][$city] + $params[8][$city] + $params[9][$city] + $params[10][$city] + $params[11][$city] + $params[0][$city]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'pieOneYearPerCity';
        $text = $translator->trans("Visits of the year per city");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Visits per city"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }
}