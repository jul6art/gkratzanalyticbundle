<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPerPage extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function pageSevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.page as page, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('page, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $pages = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['page'], $pages)){
                    $pages[] = $entity['page'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["page"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['page']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($pages as $page){
            $series[] = array("name" => $page, "data" => array($params[1][$page], $params[2][$page], $params[3][$page], $params[4][$page], $params[5][$page], $params[6][$page], $params[0][$page]));
        }

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'sevenDaysPerPage';
        $text = $translator->trans("Views of the week per page");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Views per page"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function pageOneWeek(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.page as page, DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('page, v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        $pages = array();
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                if(!in_array($entity['page'], $pages)){
                    $pages[] = $entity['page'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["page"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['page']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($pages as $page){
            $data[] = array($page, $params[1][$page] + $params[2][$page] + $params[3][$page] + $params[4][$page] + $params[5][$page] + $params[6][$page] + $params[0][$page]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'pieOneWeekPerPage';
        $text = $translator->trans("Views of the week per page");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Views per page"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function pageOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.page as page, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('page, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $pages = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['page'], $pages)){
                    $pages[] = $entity['page'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["page"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['page']] += $entity["records"];
            }
        }

        //set series
        $series = array();
        foreach($pages as $page){
            $series[] = array("name" => $page, "data" => array($params[1][$page], $params[2][$page], $params[3][$page], $params[4][$page], $params[5][$page], $params[6][$page], $params[7][$page], $params[8][$page], $params[9][$page], $params[10][$page], $params[11][$page], $params[0][$page]));
        }

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'oneYearPerPage';
        $text = $translator->trans("Views of the year per page");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Views per page"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function pagePieOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('a.page as page, YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('page, v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array();
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        $pages = array();
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                if(!in_array($entity['page'], $pages)){
                    $pages[] = $entity['page'];
                    $cptPages = 0;
                    while ($cptPages < 12){
                        $params[$cptPages][$entity["page"]] = 0;
                        $cptPages ++ ;
                    }
                }
                $params[$offset][$entity['page']] += $entity["records"];
            }
        }

        //set series
        $data = array();
        foreach($pages as $page){
            $data[] = array($page, $params[1][$page] + $params[2][$page] + $params[3][$page] + $params[4][$page] + $params[5][$page] + $params[6][$page] + $params[7][$page] + $params[8][$page] + $params[9][$page] + $params[10][$page] + $params[11][$page] + $params[0][$page]);
        }
        $series = array(array("type" => "pie", "name" => "total", "data" => $data));

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'pieOneYearPerPage';
        $text = $translator->trans("Views of the year per page");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Views per page"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        $chart->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true,
            "options3d" => array("enabled" => true, "alpha" => 45)
        ));
        return $chart;
    }
}