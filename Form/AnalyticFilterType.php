<?php

namespace Gkratz\AnalyticBundle\Form;

use Gkratz\AnalyticBundle\Form\Standard\DatePickerRangeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/12/2016
 * Time: 23:41
 */
class AnalyticFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var  $em \Doctrine\ORM\EntityManager */
        $em = $options['em'];

        //get distinct pages
        $pages = $em->getRepository(\AppBundle\Entity\Analytic::class)->createQueryBuilder('a')
            ->groupBy('a.page')
            ->addOrderBy('a.page')
        ;
        $pOb = $pages->getQuery()->getResult();
        $p = [];
        foreach($pOb as $ob){
            $p[$ob->getPage()] = $ob->getPage();
        }

        //get distinct countries
        $countries = $em->getRepository(\AppBundle\Entity\Analytic::class)->createQueryBuilder('a')
            ->groupBy('a.country')
            ->addOrderBy('a.country')
            ;
        $cOb = $countries->getQuery()->getResult();
        $c = [];
        foreach($cOb as $ob){
            $c[$ob->getCountry()] = $ob->getCountry();
        }

        //get distinct countries
        $languages = $em->getRepository(\AppBundle\Entity\Analytic::class)->createQueryBuilder('a')
            ->groupBy('a.language')
            ->addOrderBy('a.language')
            ;
        $lOb = $languages->getQuery()->getResult();
        $l = [];
        foreach($lOb as $ob){
            $l[$ob->getLanguage()] = $ob->getLanguage();
        }

        $builder->add('browser', ChoiceType::class, array(
            'required' => false,
            'label' => 'Browser',
            'translation_domain' => 'messages',
            'placeholder' => 'all',
            'choices' => array(
                'moz' => 'moz',
                'chr' => 'chr',
                'div' => 'div'
            ),
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('ip', TextType::class, array(
            'required' => false,
            'label' => 'Ip',
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('city', TextType::class, array(
            'required' => false,
            'label' => 'City',
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('country', ChoiceType::class, array(
            'required' => false,
            'label' => 'Country',
            'translation_domain' => 'messages',
            'placeholder' => 'all',
            'choices' => $c,
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('language', ChoiceType::class, array(
            'required' => false,
            'label' => 'Language',
            'translation_domain' => 'messages',
            'placeholder' => 'all',
            'choices' => $l,
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('page', ChoiceType::class, array(
            'required' => false,
            'label' => 'Page',
            'translation_domain' => 'messages',
            'placeholder' => 'all',
            'choices' => $p,
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('referer', TextType::class, array(
            'required' => false,
            'label' => 'Referer',
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('host', TextType::class, array(
            'required' => false,
            'label' => 'Host',
            'translation_domain' => 'messages',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('newSession', ChoiceType::class, array(
            'required' => false,
            'label' => 'Session',
            'translation_domain' => 'messages',
            'placeholder' => 'all',
            'choices' => array(
                'yes' => '1',
                'no' => '0'
            ),
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('date',
            DatePickerRangeType::class,
            array(
                'left_date_options' => array(
                    'label' => 'Start date',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime'
                    )
                ),
                'right_date_options' => array(
                    'label' => 'End date',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'type' => 'datetime'
                    )
                ),
                'required' => false
            ));
    }

    public function getBlockPrefix()
    {
        return 'analytic_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(array('em'));
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}