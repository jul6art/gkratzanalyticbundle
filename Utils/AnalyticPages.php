<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:04
 */

namespace Gkratz\AnalyticBundle\Utils;

use AppBundle\Entity\Analytic;
use Doctrine\ORM\EntityManager;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticPages extends AnalyticBase
{
    /**
     * @return Highchart
     */
    public function pagesSevenDays(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P7D'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('DAY(a.date) AS v_day, DAYOFWEEK(a.date) AS v_dow, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('v_day')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('count' => 0);
        $date = new \Datetime();
        $day = $date->format('d');
        $dow = $date->format('N');
        $params = $this->getWeekParams($translator, $sorts, $dow);

        //sort records
        foreach( $group as $entity ){
            $temp = $entity["v_dow"];
            if($temp == 1){
                $temp = 7;
            }else{
                $temp --;
            }
            if(($temp == $dow) && ($entity["v_day"] != $day)){
            }else{
                $offset = $temp - $dow;
                if($offset < 0){
                    $offset += 7;
                }
                $params[$offset]['count'] += $entity["records"];
            }
        }

        //set series
        $series = array(
            array("name" => $translator->trans("Views"),    "data" => array($params[1]['count'],$params[2]['count'],$params[3]['count'],$params[4]['count'],$params[5]['count'],$params[6]['count'],$params[0]['count'])),
        );

        //set categories
        $categories = array($params[1]['day'], $params[2]['day'], $params[3]['day'], $params[4]['day'], $params[5]['day'], $params[6]['day'], $params[0]['day']);

        //render chart
        $name = 'sevenDaysPages';
        $text = $translator->trans("Views of the week");
        $Xtext = array('text'  => $translator->trans("Days"));
        $Ytext = array('text'  => $translator->trans("Views"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }

    /**
     * @return Highchart
     */
    public function pagesOneYear(EntityManager $em, TranslatorInterface $translator){
        //set db managers
        $date1 = new \DateTime();
        $date2 = clone $date1;
        $date1->sub(new \DateInterval('P1Y'));
        $qb = $em->getRepository(Analytic::class)->createQueryBuilder('a')
            ->select('YEAR(a.date) AS v_year, MONTH(a.date) AS v_month, COUNT(a.id) AS records')
            ->where('a.date BETWEEN :date1 AND :date2')
            ->setParameters(array('date1' => $date1, 'date2' => $date2))
            ->groupBy('v_year, v_month')
            ->addOrderBy('a.date', 'desc');
        $group = $qb->getQuery()->getResult();

        //init
        $sorts = array('count' => 0);
        $date = new \Datetime();
        $month = $date->format('m');
        $year = $date->format('Y');
        $params = $this->getYearParams($translator, $sorts, $month);

        //sort records
        foreach( $group as $entity ){
            if(($entity["v_month"] == $month) && ($entity["v_year"] != $year)){
            }else{
                $offset = $entity["v_month"] - $month;
                if($offset < 0){
                    $offset += 12;
                }
                $params[$offset]['count'] += $entity["records"];
            }
        }

        //set series
        $series = array(
            array("name" => $translator->trans("Views"), "data" => array($params[1]['count'],$params[2]['count'],$params[3]['count'],$params[4]['count'],$params[5]['count'],$params[6]['count'],$params[7]['count'],$params[8]['count'],$params[9]['count'],$params[10]['count'],$params[11]['count'],$params[0]['count'])),
        );

        //set categories
        $categories = array($params[1]['month'], $params[2]['month'], $params[3]['month'], $params[4]['month'], $params[5]['month'], $params[6]['month'], $params[7]['month'], $params[8]['month'], $params[9]['month'], $params[10]['month'], $params[11]['month'], $params[0]['month']);

        //render chart
        $name = 'oneYearPages';
        $text = $translator->trans("Views of the year");
        $Xtext = array('text'  => $translator->trans("Months"));
        $Ytext = array('text'  => $translator->trans("Views"));
        $chart = $this->getChart($name, $text, $categories, $Xtext, $Ytext, $series);
        return $chart;
    }
}