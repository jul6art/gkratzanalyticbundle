/**
 * Created by VanIllaSkyPE on 09/12/2016.
 */
$('form button:reset').click(function () {
    $('form')
        .find(':radio, :checkbox').removeAttr('checked').end()
        .find('textarea, :text, select').val('');
    $(".pams-select2").val('').trigger('change');
    $('input[name="analytic_filter[date][left_date]_submit"], ' +
        'input[name="analytic_filter[date][right_date]_submit"], ' +
        'input[name="analytic_filter_date_left_date"], ' +
        'input[name="analytic_filter_date_right_date"]').val('');
    return false;
});