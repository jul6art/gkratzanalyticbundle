<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerBrowserController extends Controller
{
    /**
     * @Route("/oneWeekPerBrowser", name="analyticsOneWeekPerBrowser")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekBrowserAction(Request $request){
        $sevenDaysPerBrowser = $this->get('gkratz_analytic_charts')->sevenDaysPerBrowser();
        $pieOneWeekPerBrowser = $this->get('gkratz_analytic_charts')->pieOneWeekPerBrowser();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerBrowser.html.twig', array(
            'sevenDaysPerBrowser' => $sevenDaysPerBrowser,
            'pieOneWeekPerBrowser' => $pieOneWeekPerBrowser
        ));
    }

    /**
     * @Route("/oneYearPerBrowser", name="analyticsOneYearPerBrowser")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearBrowserAction(Request $request){
        $oneYearPerBrowser = $this->get('gkratz_analytic_charts')->oneYearPerBrowser();
        $pieOneYearPerBrowser = $this->get('gkratz_analytic_charts')->pieOneYearPerBrowser();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerBrowser.html.twig', array(
            'oneYearPerBrowser' => $oneYearPerBrowser,
            'pieOneYearPerBrowser' => $pieOneYearPerBrowser
        ));
    }
}