<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerCityController extends Controller
{
    /**
     * @Route("/oneWeekPerCity", name="analyticsOneWeekPerCity")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekCountryAction(Request $request){
        $sevenDaysPerCity = $this->get('gkratz_analytic_charts')->sevenDaysPerCity();
        $pieOneWeekPerCity = $this->get('gkratz_analytic_charts')->pieOneWeekPerCity();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerCity.html.twig', array(
            'sevenDaysPerCity' => $sevenDaysPerCity,
            'pieOneWeekPerCity' => $pieOneWeekPerCity
        ));
    }

    /**
     * @Route("/oneYearPerCity", name="analyticsOneYearPerCity")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearCountryAction(Request $request){
        $oneYearPerCity = $this->get('gkratz_analytic_charts')->oneYearPerCity();
        $pieOneYearPerCity = $this->get('gkratz_analytic_charts')->pieOneYearPerCity();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerCity.html.twig', array(
            'oneYearPerCity' => $oneYearPerCity,
            'pieOneYearPerCity' => $pieOneYearPerCity
        ));
    }
}