<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 05/12/2016
 * Time: 22:02
 */

namespace Gkratz\AnalyticBundle\Utils;

use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Translation\TranslatorInterface;

abstract class AnalyticBase
{
    /**
     * @param TranslatorInterface $translator
     * @return array
     */
    protected function getMonthsArray(TranslatorInterface $translator){
        $monthArray = array(
            $translator->trans( "Jan" ),
            $translator->trans( "Feb" ),
            $translator->trans( "Mar" ),
            $translator->trans( "Apr" ),
            $translator->trans( "May" ),
            $translator->trans( "Jun" ),
            $translator->trans( "Jul" ),
            $translator->trans( "Aug" ),
            $translator->trans( "Sep" ),
            $translator->trans( "Oct" ),
            $translator->trans( "Nov" ),
            $translator->trans( "Dec" )
        );
        return $monthArray;
    }

    /**
     * @param TranslatorInterface $translator
     * @return array
     */
    protected function getDaysArray(TranslatorInterface $translator){
        $dayArray = array(
            $translator->trans( "Mon" ),
            $translator->trans( "Tue" ),
            $translator->trans( "Wed" ),
            $translator->trans( "Thu" ),
            $translator->trans( "Fri" ),
            $translator->trans( "Sat" ),
            $translator->trans( "Sun" )
        );
        return $dayArray;
    }

    /**
     * @param TranslatorInterface $translator
     * @param $sorts
     * @param $month
     * @return array
     */
    protected function getYearParams(TranslatorInterface $translator, $sorts, $month){
        $monthArray = $this->getMonthsArray($translator);
        $month = $month;
        $cpt = 1;
        $params = array();
        while($cpt <= 12){
            $temp = array('month' => $monthArray[($month - 1)]);
            $params[$cpt - 1] = array_merge($temp, $sorts);
            if(($month + 1) == 13){
                $month = 1;
            }else{
                $month++;
            }
            $cpt++;
        }
        return $params;
    }

    /**
     * @param TranslatorInterface $translator
     * @param $sorts
     * @param $dow
     * @return array
     */
    protected function getWeekParams(TranslatorInterface $translator, $sorts, $dow){
        $dayArray = $this->getDaysArray($translator);
        $dow = $dow;
        $cpt = 1;
        $params = array();
        while($cpt <= 7){
            $temp = array('day' => $dayArray[($dow - 1)]);
            $params[$cpt - 1] = array_merge($temp, $sorts);
            if(($dow + 1) == 8){
                $dow = 1;
            }else{
                $dow++;
            }
            $cpt++;
        }
        return $params;
    }

    /**
     * @param $name
     * @param $text
     * @param $categories
     * @param $Xtext
     * @param $Ytext
     * @param $series
     * @return Highchart
     */
    protected function getChart($name, $text, $categories, $Xtext, $Ytext, $series){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = new Highchart();
        $chart->chart->renderTo($name);
        $chart->title->text($text);
        $chart->xAxis->categories($categories);
        $chart->xAxis->title($Xtext);
        $chart->yAxis->title($Ytext);
        $chart->series($series);
        return $chart;
    }
}