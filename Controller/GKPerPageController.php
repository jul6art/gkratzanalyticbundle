<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 13:19
 */

namespace Gkratz\AnalyticBundle\Controller;

use Gkratz\AnalyticBundle\Constants\Constants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * Class KNPController
 * @package Gkratz\AnalyticBundle\Controller
 */
class GKPerPageController extends Controller
{
    /**
     * @Route("/oneWeekPages", name="analyticsOneWeekPages")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekPagesAction(Request $request){
        $sevenDaysPages = $this->get('gkratz_analytic_charts')->sevenDaysPages();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPages.html.twig', array(
            'sevenDaysPages' => $sevenDaysPages
        ));
    }

    /**
     * @Route("/oneWeekPerPage", name="analyticsOneWeekPerPage")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneWeekPageAction(Request $request){
        $sevenDaysPerPage = $this->get('gkratz_analytic_charts')->sevenDaysPerPage();
        $pieOneWeekPerPage = $this->get('gkratz_analytic_charts')->pieOneWeekPerPage();

        return $this->render('GkratzAnalyticBundle:default:sevenDaysPerPage.html.twig', array(
            'sevenDaysPerPage' => $sevenDaysPerPage,
            'pieOneWeekPerPage' => $pieOneWeekPerPage
        ));
    }

    /**
     * @Route("/oneYearPerPage", name="analyticsOneYearPerPage")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearPageAction(Request $request){
        $oneYearPerPage = $this->get('gkratz_analytic_charts')->oneYearPerPage();
        $pieOneYearPerPage = $this->get('gkratz_analytic_charts')->pieOneYearPerPage();

        return $this->render('GkratzAnalyticBundle:default:oneYearPerPage.html.twig', array(
            'oneYearPerPage' => $oneYearPerPage,
            'pieOneYearPerPage' => $pieOneYearPerPage
        ));
    }

    /**
     * @Route("/oneYearPages", name="analyticsOneYearPages")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function graphOneYearPagesAction(Request $request){
        $oneYearPages = $this->get('gkratz_analytic_charts')->oneYearPages();

        return $this->render('GkratzAnalyticBundle:default:oneYearPages.html.twig', array(
            'oneYearPages' => $oneYearPages
        ));
    }
}