<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 21/11/2016
 * Time: 00:44
 */

namespace Gkratz\AnalyticBundle\Constants;

class Constants
{
    const ITEM_PER_PAGE = 25;
    const COUNTRY_CODE_UNKNOWN = 'ZZZ';
    const COUNTRY_NAME_UNKNOWN = 'Unknown country';
    const CITY_NAME_UNKNOWN = 'Unknown city';

    const DATE_FORMAT_CALENDAR = 'dd/MM/yyyy';
    const PHP_DATETIME_FORMAT_EXPORT = 'd/m/Y H:i:s';
    const PHP_DATE_FORMAT_EXPORT = 'd/m/Y';
    const PHP_TIME_FORMAT_EXPORT = 'H:i:s';
    const PHP_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const PHP_DATE_FORMAT = 'Y-m-d';
    const PHP_TIME_FORMAT = 'H:i:s';
    const FORM_DATE_FORMAT = 'yyyy-MM-dd HH:mm:ss';
}