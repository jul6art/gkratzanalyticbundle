<?php

/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 07/11/2016
 * Time: 21:15
 */

namespace Gkratz\AnalyticBundle\Utils;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;

class AnalyticCharts
{
    /** @var EntityManager */
    private $em;

    /** @var  TranslatorInterface */
    private $translator;

    private $analyticPerCountry,
            $analyticPerCity,
            $analyticPerLanguage,
            $analyticPerBrowser,
            $analyticPerPage,
            $analyticPerUser,
            $analyticPages;

    /**
     * AnalyticCharts constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, TranslatorInterface $translator){
        $this->em = $em;
        $this->translator = $translator;
        $this->analyticPerCountry = new AnalyticPerCountry;
        $this->analyticPerCity = new AnalyticPerCity;
        $this->analyticPerLanguage = new AnalyticPerLanguage;
        $this->analyticPerBrowser = new AnalyticPerBrowser;
        $this->analyticPerPage = new AnalyticPerPage;
        $this->analyticPerUser = new AnalyticPerUser;
        $this->analyticPages = new AnalyticPages;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerCountry(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCountry->countrySevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneWeekPerCountry(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCountry->countryOneWeek($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerCountry(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCountry->countryOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneYearPerCountry(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCountry->countryPieOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerCity(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCity->citySevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneWeekPerCity(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCity->cityOneWeek($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerCity(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCity->cityOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneYearPerCity(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerCity->cityPieOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerLanguage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerLanguage->languageSevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneWeekPerLanguage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerLanguage->languageOneWeek($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerLanguage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerLanguage->languageOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneYearPerLanguage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerLanguage->languagePieOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerBrowser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerBrowser->browserSevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneWeekPerBrowser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerBrowser->browserOneWeek($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerBrowser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerBrowser->browserOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneYearPerBrowser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerBrowser->browserPieOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerPage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerPage->pageSevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneWeekPerPage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerPage->pageOneWeek($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerPage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerPage->pageOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function pieOneYearPerPage(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerPage->pagePieOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPerUser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerUser->userSevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPerUser(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPerUser->userOneYear($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function sevenDaysPages(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPages->pagesSevenDays($this->em, $this->translator);
        return $chart;
    }

    /**
     * @return \Ob\HighchartsBundle\Highcharts\Highchart
     */
    public function oneYearPages(){
        /** @var  $chart \Ob\HighchartsBundle\Highcharts\Highchart */
        $chart = $this->analyticPages->pagesOneYear($this->em, $this->translator);
        return $chart;
    }
}